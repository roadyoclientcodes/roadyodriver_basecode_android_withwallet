package com.app.rms;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;

import com.app.rms.utility.MyNetworkChangeListner;
import com.app.rms.utility.NetworkUtil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.app.rms.utility.SessionManager;
import com.app.rms.utility.Utility;

public class NetworkChangeReceiver extends BroadcastReceiver
{
	private Notification notification;

	public static MyNetworkChangeListner myNetworkChangeListner=new MyNetworkChangeListner() {
		@Override
		public void onNetworkStateChanges(boolean nwStatus) {

		}
	};

	@Override
	public void onReceive(final Context context, final Intent intent)
	{
		Utility.printLog("Suri"+" onRecieve");
		String status = NetworkUtil.getConnectivityStatusString(context);
		
		String[] networkStatus = status.split(",");

		//Toast.makeText(context, networkStatus[0], Toast.LENGTH_LONG).show();
		
		Intent homeIntent=new Intent("com.app.driverapp.internetStatus");
		homeIntent.putExtra("STATUS", networkStatus[1]);
		context.sendBroadcast(homeIntent);
		Utility.printLog("PK Network Status"+status);
		if(myNetworkChangeListner!=null)
		{
			myNetworkChangeListner.onNetworkStateChanges("1".equals(networkStatus[1].trim()));
		}

		if(!"1".equals(networkStatus[1]))
		{
			sendNotification(context,networkStatus[1]);
		}
	}

	public void setMyNetworkChangeListner(MyNetworkChangeListner myNetworkChangeListner)
	{
		this.myNetworkChangeListner=myNetworkChangeListner;
	}
	private void sendNotification(Context context, String staus)
	{
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

		String title = context.getString(R.string.app_name);
		SessionManager sessionManager = new SessionManager(context);
		Intent notificationIntent;
		notificationIntent = new Intent(context, MainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
		if ("1".equals(staus))
		{
			notification = new Notification(icon, "Internet connected", when);
			notification.setLatestEventInfo(context, title, "Internet connected", intent);
		}
		else{
			notification = new Notification(icon, "No network connection found.", when);
			notification.setLatestEventInfo(context, title, "No network connection found.", intent);
		}

		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		notification.defaults |= Notification.DEFAULT_SOUND;

		// Vibrate if vibrate is enabled
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		if(sessionManager.isUserLogdIn())
		{
			notificationManager.notify(7,notification);
		}

	}

	/*public static class NetworkNt2
	{
		public static MyNetworkChangeListner myNetworkChangeListner;

		public NetworkNt2(MyNetworkChangeListner myNetworkChangeListner)
		{
			this.myNetworkChangeListner=myNetworkChangeListner;
		}
	}*/
}
