package com.app.rms.calander;

import java.io.Serializable;

public class AppointmentDtlsData implements Serializable
{
	/*"appt":[
{
"pPic":"aa_default_profile_pic.gif",
"email":"akbar@gmail.com",
"statCode":"9",
"status":"Booking completed.",
"fname":"Hshdh",
"apntTime":"12:03 pm",
"bid":"1454",
"apptDt":"2016-03-07 12:03:27",
"addrLine1":"47, 1st Main Rd, RBI Colony, Hebbal",
"payStatus":"1",
"dropLine1":"47, 1st Main Road, RBI Colony, Hebbal, Bengaluru, Karnataka 560024, India",
"duration":"2.42",
"distance":"0",
"amount":"25"
}
]*/


	private String dropLine1;

	private String pPic;

	private String status;

	private String additional_info;

	private String apptDt;

	private String TimeFee;

	private String statCode;

	private String amount;

	private String addrLine1;

	private String distance;

	private String duration;

	private String base_fare;

	private String payStatus;

	private String email;

	private String DistanceFee;

	private String airport_fee;

	private String bid;

	private String fname;

	private String apntTime;

	private String tip_amount;

	private String walletDeucted;



	public String getDropLine1() {
		return dropLine1;
	}

	public void setDropLine1(String dropLine1) {
		this.dropLine1 = dropLine1;
	}

	public String getpPic() {
		return pPic;
	}

	public void setpPic(String pPic) {
		this.pPic = pPic;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAdditional_info() {
		return additional_info;
	}

	public void setAdditional_info(String additional_info) {
		this.additional_info = additional_info;
	}

	public String getApptDt() {
		return apptDt;
	}

	public void setApptDt(String apptDt) {
		this.apptDt = apptDt;
	}

	public String getTimeFee() {
		return TimeFee;
	}

	public void setTimeFee(String timeFee) {
		TimeFee = timeFee;
	}

	public String getStatCode() {
		return statCode;
	}

	public void setStatCode(String statCode) {
		this.statCode = statCode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAddrLine1() {
		return addrLine1;
	}

	public void setAddrLine1(String addrLine1) {
		this.addrLine1 = addrLine1;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getBase_fare() {
		return base_fare;
	}

	public void setBase_fare(String base_fare) {
		this.base_fare = base_fare;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDistanceFee() {
		return DistanceFee;
	}

	public void setDistanceFee(String distanceFee) {
		DistanceFee = distanceFee;
	}

	public String getAirport_fee() {
		return airport_fee;
	}

	public void setAirport_fee(String airport_fee) {
		this.airport_fee = airport_fee;
	}

	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getApntTime() {
		return apntTime;
	}

	public void setApntTime(String apntTime) {
		this.apntTime = apntTime;
	}

	public String getTip_amount() {
		return tip_amount;
	}

	public void setTip_amount(String tip_amount) {
		this.tip_amount = tip_amount;
	}

	public String getWalletDeucted() {
		return walletDeucted;
	}

	public void setWalletDeucted(String walletDeucted) {
		this.walletDeucted = walletDeucted;
	}
}
