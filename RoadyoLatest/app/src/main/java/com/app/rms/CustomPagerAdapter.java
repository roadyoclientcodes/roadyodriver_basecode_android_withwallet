package com.app.rms;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


/**
 * Created by embed on 7/4/16.
 */
class CustomPagerAdapter extends FragmentStatePagerAdapter
{

    Context mContext;
    final int PAGE_COUNT = 2;
    Bundle bundle=new Bundle();

    private String tabTitles[] = new String[]{"PAYMENT LOGS", "TRIPS"};
    public CustomPagerAdapter(FragmentManager fm) {
        super(fm);
        //bundle.putSerializable("TRIP_DETAILS",mMasterTripDetails);
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position){

            case 0:
                Fragment mPaymentLocsFragment= new PaymentLocsFragment();
                return mPaymentLocsFragment;

            case 1:
                Fragment mTripsFragment= new TripsFragment();
                return mTripsFragment;

            default: return null;
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }


}