package com.app.rms;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.app.rms.pojo.AppointmentDetailList;
import com.app.rms.pojo.UpdateCustomerWallet;
import com.app.rms.response.AppointmentData;
import com.app.rms.response.AppointmentDetailData;
import com.app.rms.response.GeocodingResponse;
import com.app.rms.response.UpdateAppointmentDetail;
import com.app.rms.utility.*;

import org.json.JSONArray;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.javapapers.android.maps.path.HttpConnection;
import com.javapapers.android.maps.path.PathJSONParser;
import com.pubnub.api.PubNub;
import com.squareup.picasso.Picasso;


@SuppressLint("DefaultLocale")
public class PassengerDroppedActivity extends FragmentActivity implements OnClickListener, NetworkNotifier
{
	private TextView passenger_name,passengerDropoffAddress,bookingid,timer_text,network_text,actionbar_textview;
	private TextView distance;
	//private Button cancel;
	private ImageView passenger_phone;
	private Button passengerDropped;
	private RelativeLayout network_bar;
	//private ImageView detailimageview;
	private GoogleMap googleMap;
	//private ActionBar actionBar;
	private int selectedindex;
	private int selectedListIndex;
	private AppointmentDetailList appointmentDetailList;
	private AppointmentDetailData appointmentDetailData;
	//private LocationUpdate newLocationFinder;
	double mLatitude ;
	double mLongitude ; 
	//private double previousmLatitude ;
	//private double previousmLongitude ;
	private Marker secondmarker;
	private boolean isFirsttime;
//	private boolean issendnotificationCalled = true;
	private LatLng aptLatLng;
	private LatLng currentLatLng ;
	private ProgressDialog mdialog;
	//private LocationManager locationManager;
	//private String provider;
	SessionManager sessionManager;
	private int timecunsumedsecond = 0;
	private boolean runTimer = true;
	private String timeWhile_Paused="";
	private IntentFilter filter;
	private BroadcastReceiver receiver;
	private LocationUtil locationUtil;
	private TextView payment_type;
	private SeekBar sb;
	private RelativeLayout navigateWaze;
	private RelativeLayout navigateGoogleMap;
	private PicassoMarker marker;
	private ImageView payment_type_image;
	private TextView action_bar_title;
	private TextView tvNavigateMaps,tvNavWaze;
	private TextView tvDropPassengerTitle;
	private Typeface font,fontBold;
	private Location mCurrentLoc,mPreviousLoc;
	private JSONArray route_array=new JSONArray();
	private File mFile;
	private PubNub pubnub;
	private TextView distInMeter;
	private Dialog alertDialog;
	private TextView tvCustomerWallet;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle arg0)
	{
		super.onCreate(arg0);
		setContentView(R.layout.passengerdropped);
		overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
		pubnub=ApplicationController.getInstacePubnub();
		initLayoutId();
		sessionManager = new SessionManager(this);
		//Utility utility=new Utility();
		sessionManager.setBeginJourney(true);
		locationUtil = new LocationUtil(this,this);
		
		Bundle bundle=getIntent().getExtras();
		appointmentDetailList = (AppointmentDetailList) bundle.getSerializable(VariableConstants.APPOINTMENT);
		appointmentDetailData = appointmentDetailList.getAppointmentDetailData();
		 // Get the location manager
	    //locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	    // Define the criteria how to select the location provider -> use
	    // default
	   /* Criteria criteria = new Criteria();
	    provider = locationManager.getBestProvider(criteria, false);
		criteria.setAccuracy(Criteria.ACCURACY_COARSE);
		criteria.setCostAllowed(false); 
		provider = locationManager.getBestProvider(criteria, false);
		Location location = locationManager.getLastKnownLocation(provider);
	    // Initialize the location fields
	    if(location != null) 
	    {*/
	    	//Utility.printLog("Provider " + provider + " has been selected.");
	    	if (!isFirsttime) 
			{
				setupMap();
			}
	    	double lati =  sessionManager.getDriverCurrentLat();
			double lngi =  sessionManager.getDriverCurrentLongi();
			LatLng currentLatiLngi = new LatLng(lati,lngi);
		//secondmarker = googleMap.addMarker(new MarkerOptions().position(currentLatiLngi).title("First Point").icon(BitmapDescriptorFactory.fromResource(R.drawable.home_caricon)));
		try {
			//carMarker = googleMap.addMarker(new MarkerOptions().position(latLng));
			marker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(currentLatiLngi).title("First Point")));
			Picasso.with(this).load(VariableConstants.ImageUrl+sessionManager.getVehicleTypeUrl()).resize(75,150).into(marker);
		}catch (Exception e){e.printStackTrace();}
			distance.setText(sessionManager.getDistance()+" "+getResources().getString(R.string.km));
			/*onLocationChanged(location);
	    } */
	    sessionManager.setDropAddress(getCompleteAddressString(sessionManager.getDriverCurrentLat(), sessionManager.getDriverCurrentLongi()));
	    initActionBar();


		filter = new IntentFilter();
		filter.addAction("com.app.driverapp.internetStatus");
		filter.addAction("com.embed.anddroidpushntificationdemo11.push");
		receiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context context, Intent intent)
			{
				try
				{
					Bundle bucket=intent.getExtras();

					String status = bucket.getString("STATUS");
					try {
						String action = bucket.getString("action");
						String payload = bucket.getString("payload");
						if ("22".equals(action))
						{
							getAlertDropLoc(getResources().getString(R.string.messagetitle),payload);
						}
					}catch (Exception e)
					{
						e.printStackTrace();
					}
					if("1".equals(status))
					{
						network_bar.setVisibility(View.GONE);
					}
					else
					{
						if (!Utility.isNetworkAvailable(PassengerDroppedActivity.this))
						{
							network_bar.setVisibility(View.VISIBLE);
							return;
						}
						else if (!NetworkConnection.isConnectedFast(PassengerDroppedActivity.this))
						{
							network_bar.setVisibility(View.VISIBLE);
							network_text.setText(getResources().getString(R.string.lownetwork));
							return;
						}
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}

		};
	    
	    
	    
		/*actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayUseLogoEnabled(false);
		sessionManager.setIsFlagForOther(true);
		actionBar.setIcon(android.R.color.transparent);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#333333")));

		//Typeface robotoBoldCondensedItalic = Typeface.createFromAsset(getAssets(), "fonts/Zurich Condensed.ttf");
		try 
		{
			int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
			TextView actionBarTitleView = (TextView) getWindow().findViewById(actionBarTitle);

			if(actionBarTitleView != null)
			{
				//actionBarTitleView.setTypeface(robotoBoldCondensedItalic);
				//actionBarTitleView.setTextColor(android.graphics.Color.rgb(51, 51, 51));
			}
			actionBar.setTitle(getResources().getString(R.string.journeystarted));
		} 
		catch (NullPointerException e)
		{
			
		}*/
		
		//passengerDropped.setTypeface(robotoBoldCondensedItalic);
		/*double arrayOfscallingfactor[]=Scaler.getScalingFactor(PassengerDroppedActivity.this);
		double width = (160)*arrayOfscallingfactor[0];
		double height = (150)*arrayOfscallingfactor[1];
		String imageUrl =utility.getImageHostUrl(this) + appointmentDetailData.getpPic();
		Picasso.with(this) //
		.load(imageUrl) //
		.placeholder(R.drawable.ic_launcher) //
		.error(R.drawable.ic_launcher).fit()
		.resize((int)Math.round(width),(int)Math.round(height))	 //
		.into(detailimageview); */
		//passenger_name.setText(appointmentDetailList.getfName());
		passenger_name.setText(appointmentDetailData.getfName().toUpperCase());
		//passenger_phone.setText(appointmentDetailData.getMobile());

		if(appointmentDetailData.getDropAddr1().toString().isEmpty() && appointmentDetailData.getDropAddr2().toString().isEmpty())
		{
			passengerDropoffAddress.setText(getResources().getString(R.string.customerAdvice));
		}
		else
		{
			passengerDropoffAddress.setText(appointmentDetailData.getDropAddr1()+""+"\n"+appointmentDetailData.getDropAddr2());
		}

		bookingid.setText(getResources().getString(R.string.bookingidtext)+" "+appointmentDetailData.getBid());

		if ("2".equals(appointmentDetailData.getPayType())) {
			payment_type.setText(getResources().getString(R.string.cash));
			payment_type_image.setImageResource(R.drawable.on_the_way_cash_icon);

		}
		else if ("1".equals(appointmentDetailData.getPayType())) {
			payment_type.setText(getResources().getString(R.string.card));
			payment_type_image.setImageResource(R.drawable.on_the_way_card_icon);
		}
		tvCustomerWallet.setText(getResources().getString(R.string.customerWallet)+" SAR "+sessionManager.getCustomerBalance());
		selectedindex = bundle.getInt("selectedindex");
		selectedListIndex = bundle.getInt("horizontapagerIndex");

		//Typeface zurichLightCondensed = Typeface.createFromAsset(getAssets(), "fonts/Zurich Light Condensed.ttf");
		//passenger_name.setTypeface(zurichLightCondensed);
		//passenger_phone.setTypeface(zurichLightCondensed);
		//passengerDropoffAddress.setTypeface(zurichLightCondensed);
		//passenger_name.setTextColor(android.graphics.Color.rgb(51, 51, 51));
		//passengerDropoffAddress.setTextColor(android.graphics.Color.rgb(153, 153, 153));
		//newLocationFinder = new LocationUpdate();
		//newLocationFinder.getLocation(this, mLocationResult);
	}
	
	
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		finish();
	}
	
	@SuppressLint({ "NewApi", "InflateParams" })
	private void initActionBar()
	{
		getActionBar().hide();

	}

	private void startGoogleMap()
	{
		double apntLat = appointmentDetailData.getDropLat();
		double apntLong = appointmentDetailData.getDropLong();

		if(apntLat==0||apntLong==0){
			String muri="https://www.google.co.in/maps/dir///@"+sessionManager.getDriverCurrentLat()+","+sessionManager.getDriverCurrentLongi()+",15z?hl=en";
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(muri));
			intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
			startActivity(intent);
			}
		else {
			//String muri="http://maps.google.com/maps?saddr="+sessionManager.getDriverCurrentLat()+","+sessionManager.getDriverCurrentLongi()+"&daddr="+apntLat+","+apntLong;
			String muri="google.navigation:q="+apntLat+","+apntLong;
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(muri));
			intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
			startActivity(intent);
		}

	}

	private void initLayoutId()
	{
		font = Typeface.createFromAsset(getAssets(),"fonts/Lato-Regular.ttf");
		fontBold = Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");

		passenger_name = (TextView)findViewById(R.id.passenger_name);
		passenger_phone = (ImageView)findViewById(R.id.passenger_mob);
		bookingid = (TextView)findViewById(R.id.booking_id_text);
		distInMeter = (TextView)findViewById(R.id.distInMeter);
		action_bar_title=(TextView)findViewById(R.id.action_bar_title);
		tvNavigateMaps=(TextView)findViewById(R.id.tvNavigateMaps);
		tvNavWaze=(TextView)findViewById(R.id.tvNavWaze);
		tvDropPassengerTitle=(TextView)findViewById(R.id.tvDropPassengerTitle);
		passengerDropoffAddress = (TextView)findViewById(R.id.passengerfulladdress);
		payment_type= (TextView) findViewById(R.id.payment_type);
		payment_type_image= (ImageView) findViewById(R.id.payment_type_image);
		distance = (TextView)findViewById(R.id.distance_speed);
		timer_text = (TextView)findViewById(R.id.timer_text);
		network_text = (TextView)findViewById(R.id.network_text);
		tvCustomerWallet = (TextView)findViewById(R.id.tvCustomerWallet);
		network_bar = (RelativeLayout)findViewById(R.id.network_bar);

		passenger_name.setTypeface(font);
		tvNavigateMaps.setTypeface(font);
		tvNavWaze.setTypeface(font);
		network_text.setTypeface(font);
		passengerDropoffAddress.setTypeface(font);
		tvCustomerWallet.setTypeface(font);
		bookingid.setTypeface(fontBold);
		payment_type.setTypeface(fontBold);
		action_bar_title.setTypeface(fontBold);
		tvDropPassengerTitle.setTypeface(fontBold);
		distance.setTypeface(fontBold);
		timer_text.setTypeface(fontBold);

		sb = (SeekBar) findViewById(R.id.myseek);

		navigateWaze = (RelativeLayout)findViewById(R.id.navWaze);
		navigateGoogleMap = (RelativeLayout)findViewById(R.id.navGMap);

		navigateGoogleMap.setOnClickListener(this);
		navigateWaze.setOnClickListener(this);

		//passengerDropped.setOnClickListener(this);
		passenger_phone.setOnClickListener(this);
		SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		googleMap = fm.getMap();
		googleMap.setMyLocationEnabled(true);
		double size[]= Scaler.getScalingFactor(this);
		int value=(int)Math.round(250* size[0]);
		googleMap.setPadding(0,value,0,0);

		sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

				Utility.printLog("route array"+route_array.toString());
				if (seekBar.getProgress() > 65) {
					seekBar.setProgress(100);
					ConnectionDetector connectionDetector=new ConnectionDetector(PassengerDroppedActivity.this);
					if (connectionDetector.isConnectingToInternet())
					{
						new BackgroundGeocodingTaskNew().execute();
					}
					mWriteToFile(sessionManager.getBOOKING_ID());
				} else {
					seekBar.setProgress(0);

				}

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {


			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
										  boolean fromUser) {

			}
		});


	}


	/*public  double calculateDistance(double fromLatitude,double fromLongitude,double toLatitude,double toLongitude)
	{

		float results[] = new float[1];

		try 
		{
			Location.distanceBetween(fromLatitude,fromLongitude, toLatitude, toLongitude, results);
		} 
		catch (Exception e)
		{
			if (e != null)
				e.printStackTrace();
		}

		int dist = (int) results[0];
		if(dist<=0)
			return 0D;

		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		// DecimalFormat twoDForm = new DecimalFormat("#.##");
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		decimalFormat.setDecimalFormatSymbols(dfs);


		results[0]/=1000D;
		String distance = decimalFormat.format(results[0]);
		double d = Double.parseDouble(distance);
		double km = d * 1.609344f;
		// logDebug("mLocationResult  km "+km);

		double meters=km*1000.0;
		//logDebug("mLocationResult  meters "+meters);
		return meters;
	}*/

	private void setupMap()
	{
		mLatitude =appointmentDetailData.getDropLat(); 
		mLongitude =appointmentDetailData.getDropLong();
		double apntLat = sessionManager.getDriverCurrentLat();
		double apntLong =  sessionManager.getDriverCurrentLongi();
		googleMap.addMarker(new MarkerOptions().position(new LatLng(mLatitude,mLongitude)).title(getResources().getString(R.string.secondpoint)).icon(BitmapDescriptorFactory.fromResource(R.drawable.home_markers_dropoff)));
		DrowPath(apntLat,apntLong,mLatitude,mLongitude);
	}

	private void DrowPath(double sourcelatitude,double sourcelongitude,double destinationlatitude,double destinationlogitude)
	{
		double apntLat = sourcelatitude;
		double apntLong = sourcelongitude;
		if (apntLat!=0.0 && apntLong!=0.0) 
		{
			isFirsttime=true;
			//mLatitude = latitude;
			//mLongitude = longitude;  
			mLatitude = destinationlatitude;
			mLongitude = destinationlogitude;
			currentLatLng  = new LatLng(apntLat,apntLong);
			aptLatLng = new LatLng(mLatitude, mLongitude);
			//	MarkerOptions options = new MarkerOptions();
			//options.position(aptLatLng);
			//options.position(currentLatLng);
			//options.position(WALL_STREET);
			//	googleMap.addMarker(options);
			String url = getMapsApiDirectionsUrl(currentLatLng,aptLatLng);
			ReadTask downloadTask = new ReadTask();
		//	downloadTask.execute(url);
			googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,	14));
			addMarkers(currentLatLng,aptLatLng);
		}
		else
		{
			//mLatitude = latitude;
			//mLongitude = longitude;  
			mLatitude = destinationlatitude;
			mLongitude = destinationlogitude;
			// aptLatLng = new LatLng(apntLat,apntLong);
			currentLatLng = new LatLng(mLatitude, mLongitude);
			MarkerOptions options = new MarkerOptions();
			//	options.position(aptLatLng);
			options.position(currentLatLng);
			//options.position(WALL_STREET);
			googleMap.addMarker(options);
			//String url = getMapsApiDirectionsUrl();
			//ReadTask downloadTask = new ReadTask();
			//downloadTask.execute(url);
			googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,	14));
			//addMarkers();
			//android.widget.Toast.makeText(PassengerDroppedActivity.this, "Booking location  not found", android.widget.Toast.LENGTH_LONG).show();
		}
	}
	private String getMapsApiDirectionsUrl( LatLng aptLatLng,LatLng currentLatLng)
	{
		
		String waypoints = "origin="+aptLatLng.latitude+","+aptLatLng.longitude+"&"+"destination="+currentLatLng.latitude+","+currentLatLng.longitude;

		Utility.printLog("getMapsApiDirectionsUrl waypoints = "+waypoints );
		
		String sensor = "sensor=false";
		String params = waypoints + "&" + sensor;
		String output = "json";
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + params;
		return url;
		
		
		
		/*String waypoints = "waypoints=optimize:true|"+ aptLatLng.latitude + "," + aptLatLng.longitude+ "|" + "|" + currentLatLng.latitude + ","
				+ currentLatLng.longitude + "|" + WALL_STREET.latitude + ","+ WALL_STREET.longitude;

		String sensor = "sensor=false";
		String params = waypoints + "&" + sensor;
		String output = "json";
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + params;
		return url;*/
	}
	private void addMarkers(LatLng currentLatLng,LatLng  aptLatLng) 
	{
		if (googleMap != null) 
		{
			//secondmarker = googleMap.addMarker(new MarkerOptions().position(currentLatLng).title(getResources().getString(R.string.firstpoint)).icon(BitmapDescriptorFactory.fromResource(R.drawable.home_caricon)));
			googleMap.addMarker(new MarkerOptions().position(aptLatLng).title(getResources().getString(R.string.secondpoint)).icon(BitmapDescriptorFactory.fromResource(R.drawable.home_markers_dropoff)));
		}
	}
	@Override
	protected void onResume() 
	{
		super.onResume();
		if (receiver != null) 
		{
			registerReceiver(receiver, filter);
		}
		locationUtil.connectGoogleApiClient();
		//locationManager.requestLocationUpdates(provider, 1000, 1, this);
		if (sessionManager.getPassenger()) 
		{
			sessionManager.setPassenger(false);
			jobTimer();
		}
		else
		{
			if (sessionManager.getElapsedTime3() >= 0) 
			{
				timeWhile_Paused = sessionManager.getTimeWhile_Paused3();
				if(!"-1".equals(timeWhile_Paused))
				{
					long timePaused = Long.parseLong(timeWhile_Paused);
					long currentTime = System.currentTimeMillis() - timePaused;
					Utility.printLog("TIME ELAPSED EQUALS"+currentTime);
					timecunsumedsecond = sessionManager.getElapsedTime3();
					timecunsumedsecond = timecunsumedsecond+(int)Math.round(((float)(currentTime))/1000f);
				}
				else
				{
					timecunsumedsecond = (0);
				}
			}
			else
			{
				timecunsumedsecond = (0);
			}
			runTimer = true;
			jobTimer();
		}
	}
	private class ReadTask extends AsyncTask<String, Void, String> 
	{
		@Override
		protected String doInBackground(String... url) 
		{
			String data = "";
			try
			{
				HttpConnection http = new HttpConnection();
				data = http.readUrl(url[0]);
			} 
			catch (Exception e) 
			{
			}
			return data;
		}

		@Override
		protected void onPostExecute(String result) 
		{
			super.onPostExecute(result);
			new ParserTask().execute(result);
		}
	}

	private class ParserTask extends
	AsyncTask<String, Integer, List<List<HashMap<String, String>>>>
	{

		@Override
		protected List<List<HashMap<String, String>>> doInBackground(
				String... jsonData) {

			JSONObject jObject;
			List<List<HashMap<String, String>>> routes = null;

			try {
				jObject = new JSONObject(jsonData[0]);
				PathJSONParser parser = new PathJSONParser();
				routes = parser.parse(jObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return routes;
		}

		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> routes) 
		{
			try 
			{
				ArrayList<LatLng> points = null;
				PolylineOptions polyLineOptions = null;

				// traversing through routes
				for (int i = 0; i < routes.size(); i++) 
				{
					points = new ArrayList<LatLng>();
					polyLineOptions = new PolylineOptions();
					List<HashMap<String, String>> path = routes.get(i);

					for (int j = 0; j < path.size(); j++) 
					{
						HashMap<String, String> point = path.get(j);

						double lat = Double.parseDouble(point.get("lat"));
						double lng = Double.parseDouble(point.get("lng"));
						LatLng position = new LatLng(lat, lng);
						points.add(position);
					}

					polyLineOptions.addAll(points);
					polyLineOptions.width(4);
					polyLineOptions.color(Color.BLUE);
				}

				googleMap.addPolyline(polyLineOptions);
			}
			catch (Exception e) 
			{
			}
		}
	}
	

	
	@Override
	protected void onStop() {
		super.onStop();
		locationUtil.disconnectGoogleApiClient();
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		//closing transition animations
		//mWriteToFile(sessionManager.getBOOKING_ID());
		overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
		if (mdialog!=null) 
		{
			mdialog.cancel();
			mdialog.dismiss();
			mdialog=null;
		}
		unregisterReceiver(receiver);
		runTimer = false;
	}   
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		if (mdialog!=null) 
		{
			mdialog.cancel();
			mdialog.dismiss();
			mdialog=null;
		}
	}

	@Override
	public void onClick(View v)
	{
		 if (v.getId() == R.id.passenger_mob)
        {
        	selectChoice(appointmentDetailData.getMobile());
		}
		 else if (v.getId() == R.id.navGMap)
		 {
			 startGoogleMap();
		 }
		 else if (v.getId() == R.id.navWaze)
		 {
			 startWazeMap();
		 }

	}
	private void startWazeMap()
	{
		try {
			double apntLat = appointmentDetailData.getDropLat();
			double apntLong = appointmentDetailData.getDropLong();

			String url = "waze://?ll=" + apntLat + "," + apntLong + "&navigate=yes";

			//String muri = "geo: "+apntLat+","+apntLong;
			//String muri="http://maps.google.com/maps?saddr="+sessionManager.getDriverCurrentLat()+","+sessionManager.getDriverCurrentLongi()+"&daddr="+apntLat+","+apntLong;
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);
		}catch (ActivityNotFoundException e){
			Intent intent =
					new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
			startActivity(intent);
		}
	}

	private void selectChoice(final String phoneNo)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(phoneNo);

		builder.setPositiveButton("No",
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();

			}
		});
		builder.setNegativeButton("Call",
				new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
				Intent dialIntent=new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+"+"+phoneNo));
				startActivity(dialIntent);
			}
		});

		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}
	
	private void getUpdateAppointmentStatus()
	{
		Utility utility=new Utility();
		ConnectionDetector connectionDetector=new ConnectionDetector(PassengerDroppedActivity.this);
		if (connectionDetector.isConnectingToInternet()) 
		{
			String dropAddressLine1;
			String deviceid = Utility.getDeviceId(PassengerDroppedActivity.this);
			String currenttime = utility.getCurrentGmtTime();
			String sessiontoken = sessionManager.getSessionToken();
			String appntId = appointmentDetailData.getBid();
			dropAddressLine1 = appointmentDetailData.getDropAddr1();
			String passEmail=appointmentDetailData.getEmail();
			if ("".equals(dropAddressLine1))
			{
				dropAddressLine1 = "-" ;
			}
			double dropLat = sessionManager.getDriverCurrentLat();
			double dropLong = sessionManager.getDriverCurrentLongi();
			/*String distancestr = distance.getText().toString();
			String[] parts = distancestr.split(" ");
			if(parts[0].isEmpty())
			{
				parts[0]="0.0";
			}*/
			//double distanceKm = Double.valueOf(parts[0]);
			double distanceKm = sessionManager.getDistance();
			double distanceMtr = distanceKm / 0.00062137;
			//double distanceMtr = distanceKm / 0.001;

			DecimalFormat df = new DecimalFormat("#.##");
			String strDouble1 = df.format(distanceMtr);
			String cityId=sessionManager.getCityId();
			final String mparams[]={sessiontoken,deviceid,appntId,dropAddressLine1,""+dropLat,""+dropLong,strDouble1,currenttime,cityId,passEmail};
			RequestQueue queue = Volley.newRequestQueue(this);  // this = context
			String  url = VariableConstants.getUpdateAppointmentDetail_url;
			StringRequest postRequest = new StringRequest(Request.Method.POST, url,
					new Response.Listener<String>()
					{
				@Override
				public void onResponse(String response)
				{
					Utility.printLog("PassengerupdateNotificationResponse"+response);
					UpdateAppointmentDetail updateAppointmentDetail;
					Gson gson = new Gson();
					updateAppointmentDetail = gson.fromJson(response, UpdateAppointmentDetail.class);
					Utility.printLog("PassengerDetailupdateNotification"+updateAppointmentDetail);

					try 
					{
						if (mdialog!=null)
						{
							mdialog.dismiss();
							mdialog.cancel();
						}
						// 1 -> (1) Mandatory field missing
						if (updateAppointmentDetail.getErrFlag() ==0 && updateAppointmentDetail.getErrNum() == 88)
						{
							SessionManager sessionManager = new SessionManager(PassengerDroppedActivity.this);
							sessionManager.setDistance_tag(updateAppointmentDetail.getDis());

							if (updateAppointmentDetail.getApprAmount() != null && !"".equals(updateAppointmentDetail.getApprAmount())) {
								Double value = Double.parseDouble(updateAppointmentDetail.getApprAmount());
								String amount = String.format("%.2f",value) ;
								sessionManager.setAPX_AMOUNT(amount);
							}
							sessionManager.setAppointmentStatus(100);
							sessionManager.setBeginJourney(false);
						//	sessionManager.setFlagForStatusDropped(true);
							sessionManager.setIsInBooking(false);
							sessionManager.setIsPressedImonthewayorihvreached(false);
							sessionManager.setIsPassengerDropped(true);
							sessionManager.setDistance("0.0");
							sessionManager.setDistanceInDouble("0.0");
							appointmentDetailList.setCompletedPressed(true);
							appointmentDetailList.setIhaveReachedPressed(true);
							Intent intent=new Intent(PassengerDroppedActivity.this, JourneyDetailsActivity.class);
							Bundle bundle=new Bundle();
//							sessionManager.setindexofSelectedAppointment(selectedindex);
//							sessionManager.setindexofSelectedList(selectedListIndex);
							bundle.putSerializable(VariableConstants.APPOINTMENT, (Serializable) appointmentDetailList);
							bundle.putSerializable("updateApptDtls", (Serializable) updateAppointmentDetail);
							intent.putExtras(bundle);

							mInvoicePopUp(updateAppointmentDetail);
							//startActivity(intent);
							//finish();
						}
						else if (updateAppointmentDetail.getErrFlag()==1&&updateAppointmentDetail.getErrNum()==130)
						{
							// 1 -> (1) Mandatory field missing
							ErrorMessage(getResources().getString(R.string.messagetitle), updateAppointmentDetail.getErrMsg(), true);

						}
						else if (updateAppointmentDetail.getErrFlag()==1)
						{
							// 1 -> (1) Mandatory field missing
							ErrorMessage(getResources().getString(R.string.messagetitle), updateAppointmentDetail.getErrMsg(), false);

						}
					} 
					catch (Exception e) 
					{
						Utility.printLog("ExceptionException  "+e);
					}
				}
					},
					new Response.ErrorListener()
					{
						@Override
						public void onErrorResponse(VolleyError error) 
						{
							if (mdialog!=null)
							{
								mdialog.dismiss();
								mdialog.cancel();
							}
							//ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),false);
						}
					}
					) {    
				@Override
				protected Map<String, String> getParams()
				{ 
					Map<String, String>  params = new HashMap<String, String>();
					params.put("ent_sess_token",mparams[0]); 
					params.put("ent_dev_id",mparams[1]);
					params.put("ent_appnt_id", mparams[2]); 
					params.put("ent_drop_addr_line1",mparams[3]);
					//params.put("ent_drop_addr_line2", mparams[4]); 
					params.put("ent_drop_lat",mparams[4] );
					params.put("ent_drop_long", mparams[5]);
					params.put("ent_distance",mparams[6]);
					params.put("ent_date_time", mparams[7]);
					params.put("ent_cityid", mparams[8]);
					params.put("ent_pas_email", mparams[9]);
					params.put("ent_app_jsonLatLong", route_array.toString());

					Utility.printLog("PassengerGetUpdaterequest = "+params);

					return params; 
				}
			};
			int socketTimeout = 60000;//60 seconds - change to what you want
			RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
			postRequest.setRetryPolicy(policy);
			queue.add(postRequest);
		}
		else 
		{
			utility.showDialogConfirm(PassengerDroppedActivity.this,"Alert"," working internet connection required", false).show();
		}
	}
	
	private String getDurationString(int seconds) 
	{
		int hours = seconds / 3600;
		int minutes = (seconds % 3600) / 60;
		seconds = seconds % 60;
		return twoDigitString(hours) + " : " + twoDigitString(minutes) + " : " + twoDigitString(seconds);
	}
	private String twoDigitString(int number) {

		if (number == 0) {
			return "00";
		}

		if (number / 10 == 0) {
			return "0" + number;
		}
		return String.valueOf(number);
	}
	
	private void jobTimer()
	{
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run()
			{

				if(runTimer)
				{
					timecunsumedsecond = timecunsumedsecond + 1;
					timer_text.setText(""+getDurationString(timecunsumedsecond));
					handler.postDelayed(this, 1000);
				}
				else
				{
					sessionManager.setTimeWhile_Paused3(String.valueOf(System.currentTimeMillis()));
					sessionManager.setElapsedTime3(timecunsumedsecond);
				}
			}
		} , 1000);
	}
	
	/**
	 * Method for getting Drop off address from latitude and longitude
	 * @param LATITUDE
	 * @param LONGITUDE
	 * @return
	 */
	private String getCompleteAddressString(double LATITUDE, double LONGITUDE) 
	{
		String strAdd = "";
		Geocoder geocoder = new Geocoder(this, Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
			if (addresses != null) 
			{
				Address returnedAddress = addresses.get(0);
				StringBuilder strReturnedAddress = new StringBuilder("");

				for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) 
				{
					strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
				}
				strAdd = strReturnedAddress.toString();
			} 
			else 
			{
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return strAdd;
	}
	
	private class BackgroundGeocodingTaskNew extends AsyncTask<Void, Void,Void>
	{
		
		GeocodingResponse response;
		String stringResponse;
		SessionManager sm = new SessionManager(PassengerDroppedActivity.this);

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			mdialog = Utility.GetProcessDialog(PassengerDroppedActivity.this);
			mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
			mdialog.show();
			mdialog.setCancelable(false);
		}

		@Override
		protected Void doInBackground(Void...params) 
		{
			Utility.printLog("bbb BackgroundGeocodingTask");
			//String url="https://maps.googleapis.com/maps/api/geocode/json?latlng="+currentLatitude+","+currentLongitude+"&sensor=false&key="+VariableConstants.GOOGLE_API_KEY;

			String url="http://maps.google.com/maps/api/geocode/json?latlng="+sm.getDriverCurrentLat()+","+sm.getDriverCurrentLongi()+"&sensor=false";

			Utility.printLog("Geocoding url: "+url);

			stringResponse = Utility.callhttpRequest(url);
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			super.onPostExecute(result);
			
			if(stringResponse!=null)
			{
				Gson gson=new Gson();
				response=gson.fromJson(stringResponse, GeocodingResponse.class);
			}

			if(response!=null)
			{
				if(response.getStatus().equals("OK") && response.getResults()!=null && response.getResults().size()>0)
				{
					Utility.printLog("formatted address size="+response.getResults().size());
					if(response.getResults().size()>1 && !response.getResults().get(1).getFormatted_address().isEmpty())
					{
						appointmentDetailData.setDropAddr1(response.getResults().get(0).getFormatted_address());
					}
					

				}
				getUpdateAppointmentStatus();
			}
		}
	}
	@Override
	public void updatedInfo(String info) {
		
	}


	@Override
	public void locationUpdates(Location location) 
	{
		double lat =  (location.getLatitude());
	    double lng =  (location.getLongitude());
	    Utility.printLog("onLocationChanged lat="+lat+" lng="+lng+" dist: "+getResources().getString(R.string.distances)+sessionManager.getDistance());
		//Toast.makeText(PassengerDroppedActivity.this, "DIST: "+sessionManager.getDistanceInDouble(), Toast.LENGTH_SHORT).show();
		distInMeter.setText("DIST IN METER : "+sessionManager.getDistanceInDouble());

		try
		{
			if(Utility.isNetworkAvailable(this))
			{
				if(!googleMap.getUiSettings().isScrollGesturesEnabled())
				{
					googleMap.getUiSettings().setScrollGesturesEnabled(true);
					googleMap.getUiSettings().setAllGesturesEnabled(true);
					googleMap.getUiSettings().setZoomGesturesEnabled(true);
					googleMap.getUiSettings().setMyLocationButtonEnabled(true);
				}
				setCarMarker(location);

				Utility.printLog("onLocationChanged marker set");
			}
			else{
				googleMap.getUiSettings().setScrollGesturesEnabled(false);
				googleMap.getUiSettings().setMyLocationButtonEnabled(false);
				googleMap.getUiSettings().setZoomGesturesEnabled(false);
				Utility.printLog("onLocationChanged marker not set");
			}

	    	distance.setText(getResources().getString(R.string.distances)+sessionManager.getDistance()+" "+getResources().getString(R.string.km));
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("lat",lat);
			jsonObject.put("long",lng);

			if(route_array!=null){
				route_array.put(jsonObject);
			}

		} catch (Exception e) {
			Utility.printLog("onLocationChanged Exception =" +e.toString());
		}
		
	}


	@Override
	public void locationFailed(String message) {
		
	}

	private void getAlertDropLoc(String title,String message)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title);
		builder.setMessage(message);

		builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{

						// only show message
						getAppointmentDetails(appointmentDetailData.getEmail(),appointmentDetailData.getApptDt());
						dialog.dismiss();

					}
				});

		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}

	/**
	 * Method for getting appointment detail.
	 * @param email
	 * @param aptDateTime
	 */
	private void getAppointmentDetails(String email,String aptDateTime)
	{
		// animation fo accept or reject with timer
		//animatedUiHomePage(30000);
		Utility.printLog("Animation action email"+email,"Animation action Date Time"+aptDateTime);
		SessionManager sessionManager=new SessionManager(this);
		Utility utility=new Utility();
		String sessionToken=sessionManager.getSessionToken();
		String deviceid=Utility.getDeviceId(this);
		String currentDate=utility.getCurrentGmtTime();

		final String mparams[]={sessionToken,deviceid,email,aptDateTime,currentDate};
		//final ProgressDialog mdialog;
		mdialog=Utility.GetProcessDialog(this);
		mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
		mdialog.show();
		mdialog.setCancelable(false);
		RequestQueue queue = Volley.newRequestQueue(this);
		String url = VariableConstants.getAppointmentDetails_url;
		StringRequest postRequest = new StringRequest(Request.Method.POST, url,responseListenerofAppointment,errorListener1 )
		{
			@Override
			protected Map<String, String> getParams()
			{
				Map<String, String>  params = new HashMap<String, String>();
				params.put("ent_sess_token", mparams[0]);
				params.put("ent_dev_id", mparams[1]);
				params.put("ent_email", mparams[2]);
				params.put("ent_appnt_dt", mparams[3]);
				params.put("ent_date_time", mparams[4]);
				params.put("ent_user_type", "1");
				params.put("ent_user_type", "1");
				Utility.printLog("getAppointmentDetails  request "+params);
				return params;
			}
		};
		int socketTimeout = 60000;//60 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		postRequest.setRetryPolicy(policy);
		queue.add(postRequest);
	}
	Response.Listener<String> responseListenerofAppointment=new Response.Listener<String>()
	{
		@Override
		public void onResponse(String response)
		{
			if (mdialog!=null)
			{
				mdialog.dismiss();
				mdialog.cancel();
			}
			try
			{
				Utility.printLog("getAppointmentDetails  response "+response);
				AppointmentData appointmentData;
				Gson gson = new Gson();
				appointmentData=gson.fromJson(response, AppointmentData.class);

				if (appointmentData.getErrFlag()==0 && appointmentData.getErrNum() == 21)
				{
					Utility.printLog("Animation action Response Came with 0 and 21");
					sessionManager.setElapsedTime3(0);
					appointmentDetailData = appointmentData.getData();
					appointmentDetailList = new AppointmentDetailList();
					appointmentDetailList.setAppointmentDetailData(appointmentDetailData);
					passengerDropoffAddress.setText(appointmentDetailData.getDropAddr1()+""+"\n"+appointmentDetailData.getDropAddr2());

				}
				else if (appointmentData.getErrFlag()==1 && appointmentData.getErrNum()==3)
				{
					// 3 -> (1) Error occurred while processing your request.
					ErrorMessage(getResources().getString(R.string.messagetitle),appointmentData.getErrMsg(),false);
				}

				else if (appointmentData.getErrFlag()==1 && appointmentData.getErrNum()== 62 )
				{
					// 3 -> (1) Error occurred while booking not found.
					ErrorMessage(getResources().getString(R.string.messagetitle), appointmentData.getErrMsg(),true);
				}
				else if (appointmentData.getErrFlag() == 1 && appointmentData.getErrNum()==99)
				{
					ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), appointmentData.getErrMsg());
				}
				else if (appointmentData.getErrFlag() == 1 && appointmentData.getErrNum()==101)
				{
					ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle), appointmentData.getErrMsg());
				}
			}
			catch (Exception e)
			{
				Utility.printLog("getAppointmentDetailException = "+e);
				//ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),true);
			}
		}
	};
	Response.ErrorListener errorListener1=new Response.ErrorListener()
	{
		@Override
		public void onErrorResponse(VolleyError error)
		{
			if (mdialog!=null)
			{
				mdialog.dismiss();
				mdialog.cancel();
			}
			//ErrorMessage(getResources().getString(R.string.messagetitle), getResources().getString(R.string.servererror), false);
			//Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();
			Utility.printLog("Animation action Response Didnt Came Error");
		}
	};

	private void ErrorMessageForInvalidOrExpired(String title,String message)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title);
		builder.setMessage(message);


		builder.setNegativeButton(getResources().getString(R.string.okbuttontext),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						SessionManager sessionManager = new SessionManager(PassengerDroppedActivity.this);
						sessionManager.logoutUser();
						dialog.dismiss();
						Intent intent = new Intent(PassengerDroppedActivity.this, SplashActivity.class);
						stopService(new Intent(PassengerDroppedActivity.this,MyService.class));
						startActivity(intent);
						finish();

					}
				});

		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}
	private void ErrorMessage(String title,String message,final boolean flageforSwithchActivity)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(PassengerDroppedActivity.this);
		builder.setTitle(title);
		builder.setMessage(message);

		builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (flageforSwithchActivity) {
							Intent intent = new Intent(PassengerDroppedActivity.this, MainActivity.class);
							startActivity(intent);
							finish();
						} else {
							// only show message
							dialog.dismiss();
						}

					}
				});

		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	public void setCarMarker(final Location location)
	{
		mCurrentLoc=location;

		if(mPreviousLoc==null)
		{
			mPreviousLoc=location;
		}

		final float bearing = mPreviousLoc.bearingTo(mCurrentLoc);
		if(marker!=null)
		{

            /*carMarker.setPosition(new LatLng(lat, lng));
            carMarker.setAnchor(0.5f, 0.5f);
            carMarker.setRotation(bearing);
            carMarker.setFlat(true);*/

			final Handler handler = new Handler();
			final long start = SystemClock.uptimeMillis();
			Projection proj = googleMap.getProjection();
			Point startPoint = proj.toScreenLocation(new LatLng(mPreviousLoc.getLatitude(),mPreviousLoc.getLongitude()));
			final LatLng startLatLng = proj.fromScreenLocation(startPoint);
			final long duration = 500;

			final Interpolator interpolator = new LinearInterpolator();

			handler.post(new Runnable() {
				@Override
				public void run() {
					long elapsed = SystemClock.uptimeMillis() - start;
					float t = interpolator.getInterpolation((float) elapsed
							/ duration);
					double lng = t * mCurrentLoc.getLongitude() + (1 - t)
							* startLatLng.longitude;
					double lat = t * mCurrentLoc.getLatitude() + (1 - t)
							* startLatLng.latitude;
					marker.getmMarker().setPosition(new LatLng(location.getLatitude(),location.getLongitude()));
					marker.getmMarker().setAnchor(0.5f, 0.5f);
					marker.getmMarker().setRotation(bearing);
					marker.getmMarker().setFlat(true);


					if (t < 1.0) {
						// Post again 16ms later.
						handler.postDelayed(this, 16);
					} else {
						/*if (hideMarker) {
							marker.setVisible(false);
						} else {
							marker.setVisible(true);
						}*/
					}
				}
			});
		}

		mPreviousLoc=mCurrentLoc;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////


	private void mWriteToFile(String filename) {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state))
		{
			mFile = new File(Environment.getExternalStorageDirectory(), filename+".json");
		}
		else
		{
			mFile = new File(getFilesDir(),filename+".json");
		}

		FileWriter file = null;
		try {

			file = new FileWriter(mFile);
			Log.d("file content",route_array.toString());
			file.write(route_array.toString());
			file.flush();
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
////////////////////////////////////////////////////////////////////////////////////////////////////

	public void mInvoicePopUp(final UpdateAppointmentDetail updateAppointmentDetail)
	{
		final Dialog invoiceDialog = new Dialog(this);
		invoiceDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		invoiceDialog.setCanceledOnTouchOutside(false);
		invoiceDialog.setContentView(R.layout.journey_detail);
		invoiceDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		final float amount=Float.parseFloat(updateAppointmentDetail.getApprAmount());

		Button done= (Button) invoiceDialog.findViewById(R.id.btnDone);
		if(amount==0.0)
		{
			done.setText(getResources().getString(R.string.done));
		}
		done.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				invoiceDialog.dismiss();
				if(amount>0.0)
				{
					mShowAmountPopUp(updateAppointmentDetail.getApprAmount(),updateAppointmentDetail.getMininumAmtToCollect(),updateAppointmentDetail.getTripamount());
				}
				else
				{
					Intent intent=new Intent(PassengerDroppedActivity.this, MainActivity.class);
					Bundle bundle=new Bundle();
					bundle.putSerializable(VariableConstants.APPOINTMENT, (Serializable) appointmentDetailList);
					intent.putExtras(bundle);
					startActivity(intent);
					finish();
				}

			}
		});
		font = Typeface.createFromAsset(getAssets(),"fonts/Lato-Regular.ttf");
		fontBold = Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");
		TextView bid_text = (TextView) invoiceDialog.findViewById(R.id.bid_text);
		TextView tvFareHeader = (TextView) invoiceDialog.findViewById(R.id.tvFareHeader);
		TextView tvWaitingTimeHeader = (TextView) invoiceDialog.findViewById(R.id.tvWaitingTimeHeader);
		TextView tvDurationHeader = (TextView) invoiceDialog.findViewById(R.id.tvDurationHeader);
		TextView tvDistanceHeader = (TextView) invoiceDialog.findViewById(R.id.tvDistanceHeader);
		TextView tvDropUpHeader = (TextView) invoiceDialog.findViewById(R.id.tvDropUpHeader);
		TextView tvPickUpHeader = (TextView) invoiceDialog.findViewById(R.id.tvPickUpHeader);
		TextView tvEditFareHeader = (TextView) invoiceDialog.findViewById(R.id.tvEditFareHeader);
		TextView pickupLocation = (TextView) invoiceDialog.findViewById(R.id.tvPickUp);
		TextView dropoffLocation = (TextView) invoiceDialog.findViewById(R.id.tvDropUp);
		TextView distance = (TextView)invoiceDialog.findViewById(R.id.distance);
		TextView pick_time = (TextView) invoiceDialog.findViewById(R.id.pickup_time);
		TextView drop_time = (TextView) invoiceDialog.findViewById(R.id.dropoff_time);
		TextView total_time = (TextView) invoiceDialog.findViewById(R.id.tvDuration);
		TextView waiting_time = (TextView) invoiceDialog.findViewById(R.id.tvWaitingTime);
		TextView approx_fare = (TextView) invoiceDialog.findViewById(R.id.total_amount);


		TextView tvDistanceTitle = (TextView) invoiceDialog.findViewById(R.id.tvDistanceTitle);
		TextView tvTimeFareTitle = (TextView) invoiceDialog.findViewById(R.id.tvTimeFareTitle);
		TextView tvAirportTitle = (TextView) invoiceDialog.findViewById(R.id.tvAirportTitle);
		TextView tvBaseFeeTitle = (TextView) invoiceDialog.findViewById(R.id.tvBaseFeeTitle);
		TextView tvTipTitle = (TextView) invoiceDialog.findViewById(R.id.tvTipTitle);
		TextView tvTotalTitle = (TextView) invoiceDialog.findViewById(R.id.tvTipTitle);
		TextView tvWalletTitle = (TextView) invoiceDialog.findViewById(R.id.tvWalletTitle);
		TextView tvFinalTotalTitle = (TextView) invoiceDialog.findViewById(R.id.tvFinalTotalTitle);

		TextView Distance_tax_fare = (TextView) invoiceDialog.findViewById(R.id.Distance_tax_fare);
		TextView timeFare = (TextView) invoiceDialog.findViewById(R.id.timeFare);
		TextView airport_tax_fare = (TextView) invoiceDialog.findViewById(R.id.airport_tax_fare);
		TextView base_fee_fare = (TextView) invoiceDialog.findViewById(R.id.base_fee_fare);
		TextView tvTip = (TextView) invoiceDialog.findViewById(R.id.tvTip);
		TextView total_tax_fare = (TextView) invoiceDialog.findViewById(R.id.total_tax_fare);
		TextView tvWallet_fare = (TextView) invoiceDialog.findViewById(R.id.tvWallet_fare);
		TextView tvFinalTotal = (TextView) invoiceDialog.findViewById(R.id.tvFinalTotal);

		//RatingBar ratingBar = (RatingBar) invoiceDialog.findViewById(R.id.invoice_driver_rating);

		TextView tvCurrencySymbol1 = (TextView) invoiceDialog.findViewById(R.id.tvCurrencySymbol1);
		TextView tvCurrencySymbol2 = (TextView) invoiceDialog.findViewById(R.id.tvCurrencySymbol2);
		TextView tvCurrencySymbol3 = (TextView) invoiceDialog.findViewById(R.id.tvCurrencySymbol3);
		TextView tvCurrencySymbol4 = (TextView) invoiceDialog.findViewById(R.id.tvCurrencySymbol4);
		TextView tvCurrencySymbol5 = (TextView) invoiceDialog.findViewById(R.id.tvCurrencySymbol5);
		TextView tvCurrencySymbol6 = (TextView) invoiceDialog.findViewById(R.id.tvCurrencySymbol6);
		TextView tvCurrencySymbol8 = (TextView) invoiceDialog.findViewById(R.id.tvCurrencySymbol8);
		TextView tvCurrencySymbol9 = (TextView) invoiceDialog.findViewById(R.id.tvCurrencySymbol9);
		TextView tvCurrencySymbolDiscount = (TextView) invoiceDialog.findViewById(R.id.tvCurrencySymbol7);


		bid_text.setTypeface(fontBold);
		tvFareHeader.setTypeface(fontBold);
		tvWaitingTimeHeader.setTypeface(fontBold);
		tvDurationHeader.setTypeface(fontBold);
		tvDistanceHeader.setTypeface(fontBold);
		tvDropUpHeader.setTypeface(fontBold);
		tvPickUpHeader.setTypeface(fontBold);
		tvEditFareHeader.setTypeface(fontBold);

		total_tax_fare.setTypeface(font);
		tvTip.setTypeface(font);
		base_fee_fare.setTypeface(font);
		timeFare.setTypeface(font);
		Distance_tax_fare.setTypeface(font);
		tvTotalTitle.setTypeface(font);
		tvTipTitle.setTypeface(font);
		tvWalletTitle.setTypeface(font);
		tvFinalTotalTitle.setTypeface(font);
		tvBaseFeeTitle.setTypeface(font);
		tvAirportTitle.setTypeface(font);
		tvTimeFareTitle.setTypeface(font);
		tvDistanceTitle.setTypeface(font);
		airport_tax_fare.setTypeface(font);
		tvWallet_fare.setTypeface(font);
		tvFinalTotal.setTypeface(font);
		pickupLocation.setTypeface(font);
		dropoffLocation.setTypeface(font);
		distance.setTypeface(font);
		pick_time.setTypeface(font);
		drop_time.setTypeface(font);
		total_time.setTypeface(font);
		waiting_time.setTypeface(font);
		tvCurrencySymbol1.setTypeface(font);
		tvCurrencySymbol2.setTypeface(font);
		tvCurrencySymbol3.setTypeface(font);
		tvCurrencySymbol4.setTypeface(font);
		tvCurrencySymbol5.setTypeface(font);
		tvCurrencySymbol6.setTypeface(font);
		tvCurrencySymbol8.setTypeface(font);
		tvCurrencySymbol9.setTypeface(font);
		tvCurrencySymbolDiscount.setTypeface(font);

		bid_text.setText("BID : "+appointmentDetailData.getBid());

		if(!updateAppointmentDetail.getApprAmount().isEmpty())
		{
			Float amt=Float.parseFloat(updateAppointmentDetail.getApprAmount());
			approx_fare.setText(VariableConstants.CURRENCY_SYMBOL+" "+String.format("%.2f",amt));
		}


		//double dis = Double.parseDouble(updateAppointmentDetail.getDis()) * 0.00062137;
		double dis = Double.parseDouble(updateAppointmentDetail.getDis()) * 0.001;
		String disKM = String.format("%.2f",dis) ;
		distance.setText(disKM+" "+getResources().getString(R.string.km));

		int seconds=(int)updateAppointmentDetail.getDur();
		int day = (int) TimeUnit.SECONDS.toDays(seconds);
		long hours = TimeUnit.SECONDS.toHours(seconds) - (day *24);
		long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60);
		long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60);
		total_time.setText(""+hours+" H :"+minute+" M");

		pickupLocation.setText(updateAppointmentDetail.getAddr1());
		dropoffLocation.setText(updateAppointmentDetail.getDropAddr1());

		String pickup=Utility.getCurrentTime(sessionManager.getBeginTime());
		pick_time.setText(pickup);

		String drop=Utility.getCurrentTime(new Utility().getCurrentGmtTime());
		drop_time.setText(drop);

		Distance_tax_fare.setText(updateAppointmentDetail.getDistanceFee());
		timeFare.setText(updateAppointmentDetail.getTimeFee());
		if(!"".equals(updateAppointmentDetail.getAirportFree()))
		{
			airport_tax_fare.setText(updateAppointmentDetail.getAirportFree());
		}
		else {
			airport_tax_fare.setText("0");
		}
		base_fee_fare.setText(updateAppointmentDetail.getBaseFee());
		tvTip.setText(updateAppointmentDetail.getTip());

		Float walletAmount=Float.parseFloat(updateAppointmentDetail.getTripamount())-Float.parseFloat(updateAppointmentDetail.getApprAmount());

		total_tax_fare.setText(updateAppointmentDetail.getTripamount());
		tvWallet_fare.setText(String.format("%.2f",walletAmount));
		tvFinalTotal.setText(updateAppointmentDetail.getApprAmount());

		invoiceDialog.show();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////
	public void mShowAmountPopUp(final String apprAmount, final String mininumAmtToCollect, final String tripamount)
	{
		alertDialog = new Dialog(this);
		alertDialog.setCancelable(false);
		alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alertDialog.setContentView(R.layout.layout_amount_pop_up);
		alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		final EditText editText= (EditText) alertDialog.findViewById(R.id.etamount);
		final TextView tripAmount= (TextView) alertDialog.findViewById(R.id.tripAmount);
		Button submit= (Button) alertDialog.findViewById(R.id.submit);

		tripAmount.setText("SAR "+apprAmount);


		//final float balance=sessionManager.getCustomerBalanceLimit();
		//final float TripAmount=Float.parseFloat(apprAmount);
		final float mininumAmtt=Float.parseFloat(mininumAmtToCollect);

		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view)
			{
				if(!editText.getText().toString().isEmpty())
				{
					float value=Float.parseFloat(editText.getText().toString());
					if(value>=0) {
						if (value >= mininumAmtt) {
							updateCustomerWallet(value, apprAmount, tripamount);
							alertDialog.dismiss();
						} else {
							ErrorMessage(getResources().getString(R.string.messagetitle), getResources().getString(R.string.customer_shuld_pay_min_amt)+" SAR " + mininumAmtToCollect, false);
						}

					}

				}

			}
		});
		alertDialog.show();
	}

////////////////////////////////////////////////////////////////////////////////////////////////////
	private void updateCustomerWallet(final float amount, String apprAmount, String tripamount)
	{
		Utility utility=new Utility();

		ConnectionDetector connectionDetector=new ConnectionDetector(PassengerDroppedActivity.this);
		if (connectionDetector.isConnectingToInternet())
		{
			String deviceid = Utility.getDeviceId(PassengerDroppedActivity.this);

			final String sessiontoken = sessionManager.getSessionToken();
			final String mparams[]={sessiontoken,deviceid,""+amount,appointmentDetailData.getBid(),appointmentDetailData.getEmail(),tripamount};
			mdialog = Utility.GetProcessDialog(PassengerDroppedActivity.this);
			mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
			mdialog.show();
			mdialog.setCancelable(false);
			RequestQueue queue = Volley.newRequestQueue(this);  // this = context
			String  url = VariableConstants.UpdateCustomerWallet;
			final String balanceType;
			if(amount>0)
			{
				balanceType="0";
			}else
			{
				balanceType="1";
			}
			StringRequest postRequest = new StringRequest(Request.Method.POST, url,
					new Response.Listener<String>()
					{
						@Override
						public void onResponse(String response)
						{
							try
							{

								Utility.printLog("UpdateCustomerwallet response"+response);
								Gson gson=new Gson();
								UpdateCustomerWallet updateCustomerWallet=gson.fromJson(response,UpdateCustomerWallet.class);

								if (mdialog!=null)
								{
									mdialog.dismiss();
									mdialog.cancel();
								}
								if("0".equals(updateCustomerWallet.getErrFlag()) && "69".equals(updateCustomerWallet.getErrNum()))
								{
									Intent intent=new Intent(PassengerDroppedActivity.this, MainActivity.class);
									Bundle bundle=new Bundle();
									bundle.putSerializable(VariableConstants.APPOINTMENT, (Serializable) appointmentDetailList);
									intent.putExtras(bundle);
									startActivity(intent);
									finish();
								}
								else if("0".equals(updateCustomerWallet.getErrFlag()))
								{
									ErrorMessage(getResources().getString(R.string.messagetitle),updateCustomerWallet.getErrMsg(),false);
								}

							}
							catch (Exception e)
							{
								Utility.printLog("ExceptionException"+e);
								//ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),false);
							}
						}
					},
					new Response.ErrorListener()
					{
						@Override
						public void onErrorResponse(VolleyError error)
						{
							if (mdialog!=null)
							{
								mdialog.dismiss();
								mdialog.cancel();
							}
							//ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),false);
						}
					}
			) {
				@Override
				protected Map<String, String> getParams()
				{
					Map<String, String>  params = new HashMap<String, String>();
					params.put("ent_sess_token",mparams[0]);
					params.put("ent_dev_id",mparams[1]);
					params.put("ent_amount", mparams[2]);
					params.put("ent_appnt_id",mparams[3]);
					params.put("ent_balance_type",balanceType);
					params.put("ent_email",mparams[4]);
					params.put("ent_actual_amt",mparams[5]);
					Utility.printLog("get Notification request9 = "+params);

					return params;
				}
			};
			int socketTimeout = 60000;//60 seconds - change to what you want
			RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
			postRequest.setRetryPolicy(policy);
			queue.add(postRequest);
		}
		else
		{
			utility.showDialogConfirm(PassengerDroppedActivity.this,"Alert"," working internet connection required", false).show();
		}
	}
}


