package com.app.rms.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateAppointmentDetail implements Serializable
{
	
	/*"errNum":"88",
    "errFlag":"0",
    "errMsg":"Details updated!",
    "calculatedAmount":"0.035",
    "apprAmount":"20"*/
	@SerializedName("errNum")
	private int errNum;
	@SerializedName("errFlag")
	private int errFlag;
	@SerializedName("errMsg")
	private String errMsg;
	@SerializedName("apprAmount")
	private String apprAmount;
	@SerializedName("dis")
	private String dis;
	@SerializedName("tip")
	private String tip;
	@SerializedName("discount")
	private String discount;
	@SerializedName("discountType")
	private String discountType;
	@SerializedName("DistanceFee")
	private String DistanceFee;
	@SerializedName("TimeFee")
	private String TimeFee;
	@SerializedName("baseFee")
	private String baseFee;
	@SerializedName("addr1")
	private String addr1;
	@SerializedName("dropAddr1")
	private String dropAddr1;
	@SerializedName("AirportFree")
	private String AirportFree;
	@SerializedName("dur")
	private double dur;
	@SerializedName("MininumAmtToCollect")
	private String MininumAmtToCollect;
	@SerializedName("tripamount")
	private String tripamount;


	public double getDur() {
		return dur;
	}

	public void setDur(double dur) {
		this.dur = dur;
	}

	public int getErrNum() {
		return errNum;
	}

	public void setErrNum(int errNum) {
		this.errNum = errNum;
	}

	public int getErrFlag() {
		return errFlag;
	}

	public void setErrFlag(int errFlag) {
		this.errFlag = errFlag;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getApprAmount() {
		return apprAmount;
	}

	public void setApprAmount(String apprAmount) {
		this.apprAmount = apprAmount;
	}

	public String getDis() {
		return dis;
	}

	public void setDis(String dis) {
		this.dis = dis;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getDistanceFee() {
		return DistanceFee;
	}

	public void setDistanceFee(String distanceFee) {
		DistanceFee = distanceFee;
	}

	public String getTimeFee() {
		return TimeFee;
	}

	public void setTimeFee(String timeFee) {
		TimeFee = timeFee;
	}

	public String getBaseFee() {
		return baseFee;
	}

	public void setBaseFee(String baseFee) {
		this.baseFee = baseFee;
	}

	public String getAddr1() {
		return addr1;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public String getDropAddr1() {
		return dropAddr1;
	}

	public void setDropAddr1(String dropAddr1) {
		this.dropAddr1 = dropAddr1;
	}

	public String getAirportFree() {
		return AirportFree;
	}

	public void setAirportFree(String airportFree) {
		AirportFree = airportFree;
	}

	public String getMininumAmtToCollect() {
		return MininumAmtToCollect;
	}

	public void setMininumAmtToCollect(String mininumAmtToCollect) {
		MininumAmtToCollect = mininumAmtToCollect;
	}

	public String getTripamount() {
		return tripamount;
	}

	public void setTripamount(String tripamount) {
		this.tripamount = tripamount;
	}
}
