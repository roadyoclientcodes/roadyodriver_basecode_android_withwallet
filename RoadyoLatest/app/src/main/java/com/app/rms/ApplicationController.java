package com.app.rms;

import com.app.rms.utility.VariableConstants;

import java.util.ArrayList;

import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
public class ApplicationController extends android.app.Application
{
	private static PubNub pubnub,pubnub2;
	public static PNConfiguration pnConfiguration;
	private static ArrayList<String>ChanneList=new ArrayList<String>();

	//private static boolean IsCurrentStatusIsIAmOntheWay;
//	private static Intent serviceIntent;
	@Override
	public void onCreate()
	{
		super.onCreate();

		//serviceIntent=new Intent(this, MyService.class);
		pnConfiguration = new PNConfiguration();
		pnConfiguration.setSubscribeKey(VariableConstants.SUB_KEY);
		pnConfiguration.setPublishKey(VariableConstants.PUB_KEY);
		pnConfiguration.setPresenceTimeoutWithCustomInterval(10,5);
		//pnConfiguration.setSecretKey("sec-c-MTZlZTg5N2ItZjViZC00NjE4LTliZTEtOTkwNzIyNDlkYzkw");
		pnConfiguration.setUuid("email");

		pubnub = new PubNub(pnConfiguration);
		pubnub2 = new PubNub(pnConfiguration);


		//pubnub = new Pubnub(VariableConstants.PUB_KEY,VariableConstants.SUB_KEY,"",true);
	}
	public static PubNub getInstacePubnub()
	{
		return pubnub;
	}
	public static ArrayList<String> getChannelList()
	{
		return ChanneList;
	}
	/*public static Intent getMyServiceInstance()
	{
		return serviceIntent;
	}*/

}
